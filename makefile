#This file is part of the GALVIN library.
#Copyright (c) 2018 Oliver Galvin <odg at riseup dot net>
#
#GALVIN is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#GALVIN is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with GALVIN.  If not, see <http://www.gnu.org/licenses/>.

#This small makefile is mainly for testing purposes, and to build a static
#library. See the comments below for details about the targets.
#Requires a C99 (or later) compiler, optionally with OpenMP support.

#Binaries
CC=gcc
AR=gcc-ar

#Optimisations
OPTS=-Ofast -march=native -flto -DNDEBUG
#Warnings
WARN=-Wall -Wextra -Wpedantic -Wmissing-prototypes -Wshadow -Wconversion
WARNEXTRA= -Wformat=2 -Wredundant-decls -Wlogical-op -fstack-protector-strong

#OpenMP support - Uncomment the -DNOMP line to not use OpenMP
#OMP=-fopenmp
OMP=-DNOMP

DEPS = galvin.h
OBJ = $(DEPS:.h=.o)
LIB = $(OBJ:.o=.a)
BIN = test constants
DOC = theory.pdf

all: $(BIN) $(DOC) $(LIB)

%.o: %.c $(DEPS)
	$(CC) -c -o $@ $< $(OPTS) $(WARN) $(OMP)

%.a: %.o
	$(AR) rcs $@ $<

#Build the test program
test: test.o $(OBJ)
	$(CC) -o $@ $^ $(OPTS) $(WARN) $(OMP)

#Build the constant-calculating program
constants: constants.o $(OBJ)
	$(CC) -o $@ $^ $(OPTS) $(WARN) $(OMP)

#Generate a pdf from the LaTeX theory document
%.pdf: %.tex
	xelatex $^ >/dev/null

#Profiling
profile:
	OPTS="$(OPTS) -pg" make -e
	#Then do: ./test && gprof test gmon.out > prof_output

#Debug build without optimisations
debug:
	OPTS="-g3 -pg" WARN="$(WARN) $(WARNEXTRA)" make -e

#Run cppcheck static analysis on any source files
check: debug
	cppcheck --enable=all --suppress=missingIncludeSystem -I. *.{c,h}
	valgrind --leak-check=full --error-exitcode=1 ./test

#Delete any build artifacts
clean:
	rm -f $(BIN) $(DOC) *.{a,o,exe,aux,log,out} *~

.PHONY: debug profile check clean
