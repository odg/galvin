# Galvin Arbitrary Length Verifiable Irrational Numbers

GALVIN is a small, arbitrary precision, floating point arithmetic C library.

It consists of one source and header file, with 24 functions and one struct.

## Building

Only a C99 compiler and libc is needed, there are no other dependencies.

Running `make` will produce these artifacts:

* `libgalvin.a`
* `test`
* `constants`

You can link with `libgalvin.a`, or use the `.c` and `.h` files directly in 
your project. The included makefile is optional, and mainly just for testing.

`test` and `constants` are example programs to run some test routines and 
compute some mathematical constants respectively.

## Usage

The header file `galvin.h` is well commented and explains how each function 
should be used. Also see `test.c` for some examples.

Comments in `galvin.c` go into more detail about how the functions work, what 
algorithms are implemented, and so on.

## License

Copyright (C) 2018-2019 Oliver Galvin

All files are released under the GPLv3+ license, see the `COPYING` file.
