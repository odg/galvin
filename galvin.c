/*This file is part of GALVIN, a small arbitrary precision arithmetic library.
  Copyright (C) 2018-2019 Oliver Galvin <odg at riseup dot net>

  GALVIN is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  GALVIN is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with GALVIN.  If not, see <http://www.gnu.org/licenses/>.
 *
  Read the header file galvin.h and test program for usage information.
  Comments here will explain the algorithms used and the internal mechanics.
 */

#include "galvin.h" //bignum, galv_*, stddef.h, inttypes.h, stdbool.h
#include <assert.h> //assertions - disabled with NDEBUG
#include <limits.h> //CHAR_BIT
#include <stdlib.h> //malloc, calloc, realloc, free
#include <string.h> //strcpy, strlen, memset
#ifndef NOMP
 #include <omp.h>   //optional OpenMP support
#endif
#ifndef static_assert
//Pre-C11 (https://pixelbeat.org/programming/gcc/static_assert.html)
 #define ASSERT_CONCAT_(a, b) a##b
 #define ASSERT_CONCAT(a, b) ASSERT_CONCAT_(a, b)
 #define static_assert(e,m) \
   ;enum { ASSERT_CONCAT(assert_line_, __LINE__) = 1/(int)(!!(e)) }
#endif
//Some useful macros
#define MAX(a,b) (a >= b ? a : b)
#define BPW (sizeof(wtype)*CHAR_BIT) //Max bits per word
#define BPD(x) ((size_t)((double)(x)*3.321928094887362347870319)) //Max bits per digit = log2(10)
#define DPB(x) ((size_t)((double)(x)*0.301029995663981195213738)) //Max digits per bit = log10(2)

/* Memory management - wrappers around the manual memory allocation functions
 ***************************************************************************/

/*free() wrapper - avoids double frees by null checking/setting*/
static inline void
safe_free(void **ptr)
{
	if(ptr != NULL && *ptr != NULL) {
		free(*ptr);
		*ptr = NULL;
	}
}
#define FREE(ptr) safe_free((void **)&ptr)

/*realloc() wrapper - check if realloc failed, free memory if it fails,
   and consistent output when size=0*/
static inline void*
nodiscard
safe_realloc(void *ptr, size_t size)
{
	if(!size) {
		return NULL;
	}
	void *temp = realloc(ptr, size);
	if(!temp) {
		FREE(ptr);
		return NULL;
	}
	ptr = temp;
	return ptr;
}
#define REALLOC(ptr, size) if(!(safe_realloc((void *)ptr, size))) {return NULL;}

/* Sizing - Safely creating/resizing bignums and strnums ensuring correctness
 ***************************************************************************/

/*Initialise a bignum structure - see theory.pdf for a formal description.
   Allocates and initialises the given number of words.*/
static inline bignum*
nodiscard
allocbignum(const size_t count)
{
	bignum *a = (bignum *)calloc(1, sizeof(bignum));
	if(!a) {
		return NULL;
	}
	a->word = (wtype *)calloc(count, sizeof(wtype));
	if(!(a->word)) {
		FREE(a);
		return NULL;
	}
	a->wnum = count;
	return a;
}

/*Add or remove words to a bignum, update the wnum, and return the success*/
static inline bignum*
nodiscard
addwords(bignum *a, const size_t n)
{
	assert(n);
	assert(a);
	/*This is similar to the nonstandard reallocarray, so we need to check
	if the multiplication will overflow before doing the actual realloc*/
	if(n > (SIZE_MAX/sizeof(wtype)) - a->wnum) {
		galv_free(a);
		return NULL;
	}
	REALLOC(a->word, sizeof(wtype)*(a->wnum + n));
	memset(a->word + a->wnum, 0, n);
	a->wnum += n;
	return a;
}

/*Free any memory allocated to a bignum*/
extern void
galv_free(bignum *a)
{
	if(a) {
		FREE(a->word);
		FREE(a);
	}
}

/* Debugging - These check certain properties and are only used in assertions
 ***************************************************************************/
#ifndef NDEBUG
/*Check the string is a valid number.
   Should match the regex: [+-]?[0-9]+(\.[0-9]+)? */
static inline bool
isvalidstrnum(const char *restrict s)
{
	size_t i;
	bool dp = false;
	bool sign = false;
	if(!s) {return false;}
	for(i=0; i<strlen(s); i++) {
		char c = s[i];
		if(c == '-' || c == '+') {
			if(sign || i > 0) {return false;}
			sign = true;
		} else if(c == '.') {
			if(dp || i == 0) {return false;}
			dp = true;
		} else if(c < '0' || c > '9') {return false;}
	}
	return true;
}

/*Check the bignum is 'normal': that there are no unnecessary words used, so
   the minimum space needed is used, and its values are valid*/
static inline bool
isnormal(const bignum *a)
{
	if(a->word[a->wnum - 1] == 0) {
		return false;
	} else if(a->dp && !(a->word[0] & 1)) {
		return false;
	} else if(a->dp > a->wnum) {
		return false;
	}
	return true;
}
#endif /* NDEBUG */

/* Extras - Small functions that are useful as part of the later main functions
 *****************************************************************************/

/*Calculates the integer part of the binary log (the MSB position) of a word
   Try to use optimised compiler intrinsics, with an efficient fallback*/
static inline size_t
wlb(const wtype n)
{
	if(!n) { //lb(0) = -inf, but return 0 to avoid undefined behaviour
		return 0;
	}
#if __GNUC__ || __clang__
	return BPW - 1 - (size_t)__builtin_clzll(n);
#elif _MSC_VER
	unsigned long index;
	_BitScanReverse64(&index, n);
	return (size_t)index;
#else
/*Portable fallback - uses a De Bruijn sequence based algorithm, see here:
https://graphics.stanford.edu/~seander/bithacks.html#IntegerLogDeBruijn*/
//TODO: Implement this
 #error "Could not find a binary log implementation!"
#endif
}

/*Return remainder when dividing digit by two*/
static inline bool
oddstoone(const char c)
{
	return c == '1' || c == '3' || c == '5' || c == '7' || c == '9' ? 1 : 0;
}

static inline bignum*
normalise(bignum *a)
{
	return a; //TODO: Write this!
}

/* String handling - operating on numbers represented as strings of digits
 ************************************************************************/

static inline char*
incrstrnum(char *restrict const s, const size_t len)
{
	char carry = 1;
	size_t i = len;
	while(carry && i--) {
		s[i] = (char)(s[i] + carry);
		if(s[i] > '9') {
			s[i] = '0';
		}
		carry = s[i] == '0' ? 1 : 0;
	}
	return s;
}

static inline char*
doublestrnum(char *restrict const s, const size_t len, const size_t nonzero)
{
	size_t i = len;
	char carry = 0;
	//Note: '0' is 48
	while(i-- > nonzero) {
		char next = (s[i]>'4') ? 1 : 0;
		s[i] = next ? (char)(2*s[i] - 58 + carry) : (char)(2*s[i] - 48 + carry);
		carry = next;
	}
	if(nonzero) {
		s[i] = (char)(carry + '0');
	}
	return s;
}

/* Getting and Setting - User functions to set a bignum based on a 'human'
   representation - strings of digits, or to extract such a string back out
 *****************************************************************************/

/*Create a bignum based on a strnum*/
extern bignum*
nodiscard
galv_set(const char *restrict const dec)
{
	assert(isvalidstrnum(dec));
	const size_t dlen = strlen(dec);
	bignum *res = allocbignum(BPD(dlen)/BPW+1);
	if(!res) {
		return NULL;
	}
	size_t skip;
	for(skip=0; dec[skip] == '0' || dec[skip] == '+' || dec[skip] == '-'; skip++);
	size_t hlen = dlen - skip;
	if(!hlen) {
		return res;
	}
	char * restrict half = (char *)malloc(hlen+1);
	if(!half) {
		galv_free(res);
		return NULL;
	}
	size_t nonzero = 0;
	size_t w = 0;
	half = strcpy(half, dec + skip);
	res->neg = (*dec == '-') ? true : false;
	wtype pot = 1;
	while(nonzero < hlen) {
		if(oddstoone(half[hlen-1])) {
			res->word[w] |= pot;
		}
		//half = halvestrnum(half, &off);
		char carry = 0;
		if(half[nonzero] == '1') {
			half[nonzero++] = '0';
			carry = 5;
		}
		for(size_t i=nonzero; half[i]; i++) {
			char old = half[i];
			half[i] = (char)((half[i] + '0') / 2 + carry);
			carry = (char)(oddstoone(old) * 5);
		}
		pot = pot << 1 | pot >> (BPW-1);
		if(pot == 1) { //New word
			w++;
		}
	}
	free(half);
	return res;
}

/*Create a bignum based on a signed variable*/
extern bignum*
nodiscard
galv_set_num(const long int x)
{
	static_assert(wmax >= LONG_MAX, "Word width is smaller than long int");
	bignum *res = allocbignum(1);
	if(!res) {
		return NULL;
	}
	res->neg = x < 0 ? true : false;
	res->word[0] = (wtype)labs(x);
	return res;
}

/*Convert the given bignum into its decimal representation, in a string*/
extern char*
galv_get(const bignum *restrict const a)
{
	size_t declen = galv_len(a);
	if(!declen) {
		return NULL; //Invalid number - no digits
	}
	if(a->neg) {
		declen++; //Make room for the minus symbol
	}
	char *dec = (char *)malloc(declen+1); //The decimal output
	if(!dec) {
		return NULL; //Out of memory
	}
	size_t nonzero = declen - 1; //The first nonzero digit in the string
	memset(dec, '0', declen);
	dec[declen] = '\0';
	if(a->wnum == 1 && a->word[0] == 0) {
		return dec; //a = 0, so we are done
	}
	dec[declen-1] = '1'; //We start with dec = 1
	size_t w = a->wnum;
	wtype pot = 1; //Power of two, calculated by bit shifts
	if(a->word[w-1] == 1) {
		/*If the biggest word is 1, nothing needs to be done for that
		  word because we initialised dec at 1*/
		w--;
	} else {
		//Start at most significant bit in most significant word
		pot <<= wlb(a->word[w-1]);
	}
	while(w--) {
		do {
			pot = pot >> 1 | pot << (BPW-1); //Circular shift
			doublestrnum(dec, declen, nonzero);
			if(a->word[w] & pot) {
				incrstrnum(dec, declen);
			}
			if(nonzero && dec[nonzero-1] != '0') {
				nonzero--;
			}
		} while(pot != 1);
	}
	if(nonzero) { //TODO: If galv_len worked properly this would be unecessary
		memmove(dec, dec+nonzero-a->neg, declen-nonzero+a->neg+1);
	}
	if(a->neg) {
		*dec = '-';
	}
	return dec;
}

/*Negate a - flip the sign of a*/
static inline bignum*
galv_neg(bignum *a) {
	if(a->wnum > 1 || a->word[0] > 0) { //Don't negate zero!
		a->neg = !(a->neg);
	}
	return a;
}

/*Three way comparison, as in strcmp.
   Returns 1 if a > b, -1 if a < b, or 0 if a == b */
extern int
galv_cmp(const bignum *a, const bignum *b)
{
	assert(isnormal(a));
	assert(isnormal(b));
	int c = 1;
	if(!a->neg && b->neg) {
		return 1;
	} else if(a->neg && !b->neg) {
		return -1;
	} else if(a->neg && b->neg) {
		c = -1;
	}
	//0 <= dp <= wnum
	if((a->wnum - a->dp) > (b->wnum - b->dp)) {
		return c;
	} else if((a->wnum - a->dp) < (b->wnum - b->dp)) {
		return -c;
	} else if(a->word[a->wnum-1] > b->word[b->wnum-1]) {
		return c;
	} else if(a->word[a->wnum-1] < b->word[b->wnum-1]) {
		return -c;
	}
	return 0;
}

/*Returns the base 10 length, the number of decimal digits in a bignum*/
extern size_t
galv_len(const bignum *a)
{
	//If there are no words, the number doesn't exist, so has no digits
	if(!a || a->wnum == 0) {
		return 0;
	}
	assert(isnormal(a));
	const size_t lastwordbits = wlb(a->word[a->wnum-1]) + 1;
	//Zero has 1 digit
	if(!lastwordbits && a->wnum == 1) {
		return 1;
	}
	assert((SIZE_MAX - lastwordbits)/BPW + 1 > a->wnum);
	const size_t bits = (a->wnum - 1) * BPW + lastwordbits;
	size_t len = DPB(bits) + 1;
	//bignum *ten = galv_set_num(10);
	//bignum *poten = galv_pow(ten, len-1);
	//if(galv_cmp(a, poten) == -1) {
		//if a < 10^(len-1), then decrement len
	//	len--;
	//}
	//galv_free(ten);
	//galv_free(poten);
	//log10 of a = log10(words) - BPW*a->dp*DPB
	if(a->dp) {
		len -= DPB(BPW) * a->dp;
	}
	return len;
}

/* The Basics - Essential user functions for doing basic arithmetic on bignums
 ****************************************************************************/

/*Raw subtraction function, used internally by wrappers.
   Returns a - b, ignoring the neg variable. Assumes a > b > 0 */
static inline bignum*
raw_sub(const bignum *a, const bignum *b)
{
	assert(galv_cmp(a, b) == 1);
	const size_t maxwnum = MAX(a->wnum, b->wnum);
	bignum *res = allocbignum(maxwnum);
	if(!res) {
		return NULL;
	}
	res->dp = MAX(a->dp, b->dp);
	int carry = 1;
	size_t ai=0, bi=0, ri=0;
	while(ri<maxwnum) {
		res->word[ri] = (wtype)carry;
		carry = 0;
		if(ai >= res->dp - a->dp && ai < a->wnum) {
			carry = (res->word[ri] > wmax - a->word[ai]) ? 1 : 0;
			res->word[ri] += a->word[ai];
			ai++;
		}
		if(bi >= res->dp - b->dp && bi < b->wnum) {
			wtype not = ~(b->word[bi]);
			if(!carry) {
				carry = (res->word[ri] > wmax - not) ? 1 : 0;
			}
			res->word[ri] += not;
			bi++;
		}
		ri++;
	}
	//Ignore any carry remaining, and normalise the output if necessary
	return normalise(res);
}

/*Raw addition function, used internally by wrappers.
   Returns a + b, ignoring the neg variable.*/
static inline bignum*
raw_add(const bignum *a, const bignum *b)
{
	const size_t maxwnum = MAX(a->wnum, b->wnum);
	bignum *res = allocbignum(maxwnum);
	if(!res) {
		return NULL;
	}
	res->dp = MAX(a->dp, b->dp);
	int carry = 0;
	size_t ai=0, bi=0, ri=0;
	while(ri<maxwnum) {
		res->word[ri] = (wtype)carry;
		carry = 0;
		if(ai >= res->dp - a->dp && ai < a->wnum) {
			carry = (res->word[ri] > wmax - a->word[ai]) ? 1 : 0;
			res->word[ri] += a->word[ai];
			ai++;
		}
		if(bi >= res->dp - b->dp && bi < b->wnum) {
			if(!carry) {
				carry = (res->word[ri] > wmax - b->word[bi]) ? 1 : 0;
			}
			res->word[ri] += b->word[bi];
			bi++;
		}
		ri++;
	}
	if(carry) {
		if(!addwords(res, 1)) {
			galv_free(res);
			return NULL;
		}
		res->word[maxwnum] = 1;
	}
	return res;
}

/*External subtraction wrapper for the raw function, to ensure correct sign*/
extern bignum*
galv_sub(const bignum *a, const bignum *b)
{
	int cmp = galv_cmp(a, b);
	if(cmp == 0) {                        // a - b = 0
		return allocbignum(1);
	} else if(cmp == 1) {
		if(!a->neg && b->neg) {       // a - - |b| = a + |b|
			return raw_add(a, b);
		} else if(a->neg && b->neg) { // -|a| - -|b| = -(|a| - |b|)
			return galv_neg(raw_sub(a, b));
		} else {                      // |a| - |b| = a - b
			return raw_sub(a, b);
		}
	} else {                              // a - b = -(b - a)
		return galv_neg(galv_sub(b, a));
	}
}

/*External addition wrapper for the raw function, to ensure correct sign*/
extern bignum*
galv_add(const bignum *a, const bignum *b)
{
	if(a->neg && !b->neg) {
		return raw_sub(b, a);
	} else if(!a->neg && b->neg) {
		return raw_sub(a, b);
	} else if(a->neg && b->neg) {
		return galv_neg(raw_add(a, b));
	}
	return raw_add(a, b);
}

/*Multiplication - picks the appropriate algorithm based on size, and ensures
   correct sign. */
extern bignum*
galv_mul(const bignum *a, const bignum *b)
{
	bignum *res = allocbignum(1);
	if(!res) {
		return NULL;
	}
	//TODO: implement multiplication algorithms
	if(a->neg ^ b->neg) {
		return galv_neg(res);
	}
	return res;
}

/*
extern inline bignum*
galv_div(const bignum *a, const bignum *b)
{

}
*/
/*Implement: exponentiation by squaring, different multiplication/division
  algorithms, binary/decimal/natural logs
 */
