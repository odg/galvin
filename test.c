#include "galvin.h"
#include <stdlib.h> //free
#include <stdio.h>  //printf
#include <string.h> //strcmp
#include <limits.h> //SIZE_MAX

static inline void
showcompiler(void)
{
	printf("Compiled with: ");
        #if defined(__clang__)
                printf("Clang %i.%i", __clang_major__, __clang_minor__);
        #elif defined(__GNUC__)
                printf("GCC %i.%i", __GNUC__, __GNUC_MINOR__);
        #elif defined(__MINGW32__)
                printf("MinGW %i.%i", __MINGW32_MAJOR_VERSION,
                                      __MINGW32_MINOR_VERSION);
	#elif defined(_MSC_VER)
		printf("Microsoft Visual C/C++ Compiler %i", _MSC_VER);
	#elif defined(SDCC)
		printf("Small Device C Compiler %i", SDCC);
	#elif defined(__INTEL_COMPILER)
		printf("Intel C/C++ Compiler %i", __INTEL_COMPILER);
	#elif defined(__COMPCERT__)
		printf("CompCert");
	#elif defined(__TINYC__)
		printf("Tiny C Compiler");
        #else
                printf("Unknown");
        #endif
	//Others: Borland, Turbo, Watcom, AMD, Oracle, IBM
	printf("\n");
}

static inline void
showbignum(const bignum *a)
{
	printf("Displaying bignum struct:\n");
	printf(" "wfmt" words, ", a->wnum);
	if(a->neg) {
		printf("negative number\n");
	} else {
		printf("positive number\n");
	}
	printf(" "wfmt" words after decimal point\n", a->dp);
	printf(" Words:\n");
	for(size_t i=0; i<a->wnum; i++) {
		printf("  "wfmt"\n", a->word[i]);
	}
	printf("\n");
}

static inline int
checkpass(const char *res, const char *exp)
{
	printf("Expected value: %s\n", exp);
	printf("Returned value: %s\n", res);
	if(!strcmp(res, exp)) {
		printf("Test passed!\n\n");
		return 0;
	} else {
		printf("Test failed.\n\n");
		return 1;
	}
}

static inline int
test1(void)
{
	printf("Getting and setting a large bignum\n");
	const char *str = "45028376592836528365028060948750932480535283650283560826028376502650827650873052873065";
	bignum *a = galv_set(str);
	char *ret = galv_get(a);
	int res = checkpass(str, ret);
	free(ret);
	galv_free(a);
	return res;
}

static inline int
test2(void)
{
	printf("Addition and subtraction\n");
	const char *sum = "4603770011386257722856344";
	bignum *a = galv_set("4075293487509324875293487");
	bignum *b = galv_set("528476523876932847562857");

	bignum *c = galv_add(a, b);
	char *ret_sum = galv_get(c);
	int res = checkpass(ret_sum, sum);
	if(!res) {
		const char *diff = "3546816963632392027730630";
		bignum *d = galv_sub(a, b);
		char *ret_diff = galv_get(d);
		res = checkpass(ret_diff, diff);
		galv_free(d);
		free(ret_diff);
	}

	galv_free(a);
	galv_free(b);
	galv_free(c);
	free(ret_sum);
	return res;
}

int
main(void)
{
	printf("GALVIN Library Test Routines\n");
	printf("----------------------------\n\n");
	showcompiler();
	printf("Constants:\n");
	printf(" bits per byte: %u\n", CHAR_BIT);
	printf(" wtype - %zu bits, max value: "wfmt"\n", \
	       CHAR_BIT*sizeof(wtype), wmax);
	printf(" size_t - %zu bits, max value: %zu\n\n", \
	       CHAR_BIT*sizeof(size_t), SIZE_MAX);

	int (*tests[])(void) = {&test1, &test2, NULL};

	for(int i=0; tests[i]; i++) {
		printf("Test %i - ", i);
		if((tests[i])()) {
			return EXIT_FAILURE;
		}
	}
	return EXIT_SUCCESS;
}
