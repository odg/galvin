/*This file is part of the GALVIN library.
  Copyright (c) 2018 Oliver Galvin <odg at riseup dot net>

  GALVIN is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  GALVIN is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with GALVIN.  If not, see <http://www.gnu.org/licenses/>.
 *
 This program calculates mathematical constants, using the GALVIN library.
  All are calculated to a precision of d decimal digits

  So far 5 constants are implemented:
  pi, e, golden ratio, sqrt(2), Euler-Mascheroni */

#include "galvin.h"
#include <stdio.h>

/*An implementation of the Chudnovsky algorithm to calculate π (pi)
Useful/interesting references:
 * https://en.wikipedia.org/wiki/Chudnovsky_algorithm
 * http://mathworld.wolfram.com/PiFormulas.html
 * https://www.ncbi.nlm.nih.gov/pmc/articles/PMC298242
 * https://arxiv.org/abs/1809.00533 */
static inline int
pi(const wtype d)
{
	#define DPI 14.1816474627 //Digits per iteration log10(151931373056000)
	printf("Calculating pi to " wfmt " digits.\n", d);
	printf("Initialising constants...\n");
	size_t iter = (size_t)((double)d/DPI) + 1;

/*	bignum *tmp;
	bignum *C = galv_set("10005");
	C = galv_root(C, 2, true);
	tmp = galv_set("426880");
	C = galv_add(C, tmp, true);
	galv_free(tmp);

	bignum *L = galv_set("13591409");
	bignum *Ladd = galv_set("545140134");

	bignum *X = galv_set("1");
	bignum *Xmul = galv_set("-262537412640768000");

	bignum *K = galv_set("6");
	bignum *Kadd = galv_set("12");

	bignum *M = galv_set("1");
	bignum *Mmul;

	bignum *res, term;
*/
	printf("Excecuting %zu iterations...\n", iter);
/*	for(size_t i=1; i<=iter; i++) {
		term = galv_mul(M, L);
		term = galv_div(term, X);
		res = galv_add(res, term);

		L = galv_add(L, Ladd);
		X = galv_mul(X, Xmul);
		K = galv_add(K, Kadd);

		Mmul = galv_pow(K, 3);
		tmp = galv_set("16");
		tmp = galv_mul(tmp, K);
		Mmul = galv_add(Mmul, tmp);
		galv_free(tmp);
		tmp = galv_set_num(i+1);
		tmp = galv_pow(tmp, 3);
		Mmul = galv_div(Mmul, tmp);
		M = galv_mul(M, Mmul);

		galv_free(tmp);
		galv_free(Mmul);
		galv_free(term);
	}

	printf("Final calculations...\n");
	res = galv_recip(res);
	res = galv_mul(res, C);

	printf("Converting from decimal to binary...\n");
	char *out = galv_get(res);

	printf("Freeing memory...\n");
	galv_free(C);
	galv_free(L);
	galv_free(Ladd);
	galv_free(X);
	galv_free(Xmul);
	galv_free(K);
	galv_free(Kadd);
	galv_free(M);
	galv_free(Mmul);
*/
	printf("Done.\n");
	return 0;
}

int
main(void)
{
	/*Constants:
	e
	Golden ratio (φ) = (sqrt(5) + 1)/2
	sqrt(2)
	Euler-Mascheroni (γ)

	Apery's constant = zeta(3)
	Catalan's constant (G)

	zeta(5)
	log(2)
	log(10)
	gamma(1/4)
	*/
	return pi(1000000);
}
