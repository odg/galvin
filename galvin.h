/*This file is part of GALVIN, a small arbitrary precision arithmetic library.
  Copyright (c) 2018-2019 Oliver Galvin <odg@riseup.net>

  GALVIN is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  GALVIN is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with GALVIN.  If not, see <http://www.gnu.org/licenses/>.
 *
  Hopefully the comments here are sufficiently clear to explain how to
  use the library, for more details read the .c source file, or email me.*/

#ifndef __GALVIN_H__
#define __GALVIN_H__

#ifdef __cplusplus // If included in C++ code
extern "C" {
#endif

#include <stddef.h>   // size_t
#include <stdbool.h>  // Boolean type, true/false
#include <inttypes.h> // PRIuMAX, stdint.h (uintmax_t, SIZE_MAX/UINTMAX_MAX)

/* Word data type (a large unsigned integer), and useful associated values
    If you change these, make sure they match, then rebuild the library */
#define wtype uintmax_t
#define wmax UINTMAX_MAX
#define wfmt "%" PRIuMAX

/* Attributes - these can be useful, and can help optimisation, but they aren't
    portable so are safely ignored if the compiler doesn't support them*/
#ifndef __has_attribute
 #define __has_attribute(x) 0
#endif
#if __has_attribute(warn_unused_result)
 #define nodiscard __attribute__ ((warn_unused_result))
#else
 #define nodiscard
#endif
#if __has_attribute(pure)
 #define purefunc __attribute__ ((pure))
#else
 #define purefunc
#endif

/* Main structure that stores an arbitrary length/precision real number */
typedef struct bignum {
	wtype *word; // Pointer to array of words containing the digits
	size_t wnum;  // Number of words in the array
	size_t dp;    // Number of words after the decimal point
	bool neg;    // True if the number is negative, false if positive
} bignum;

/*For all functions:
 * Parameters named a or b can be any real number
 * Parameters named n can be any natural number (a positive integer)
 * Functions that return bignum pointers allocate memory, the returned pointer
    must be freed with galv_free
 * When applicable, precision in the result is up to d, a natural number
    representing the maximum number of decimal (base 10) digits used
 */

/*These are "meta" functions, used for creating and destroying bignums
 ********************************************************************/
// Create a bignum representing the number in the given string
bignum* galv_set(const char *s)
 nodiscard;
// Create a bignum representing the number in the given long variable
bignum* galv_set_num(const long int x)
 nodiscard;
// Returns a string representing the given bignum in decimal (base 10)
char* galv_get(const bignum *a);
// Free any memory allocated to a bignum
void galv_free(bignum *a);

/*These functions provide the basic set of arithmetic operations
 **************************************************************/
// Add a and b
bignum* galv_add(const bignum *a, const bignum *b);
// Subtract b from a
bignum* galv_sub(const bignum *a, const bignum *b);
// Multiply a and b
bignum* galv_mul(const bignum *a, const bignum *b);
// Divide a by b
//bignum* galv_div(const bignum *a, const bignum *b);
// a modulo b
//bignum* galv_mod(const bignum *a, const bignum *b);
// a to the power of n
//bignum* galv_pow(const bignum *a, const bignum *n);
// nth root of a
//bignum* galv_root(const bignum *a, const bignum *n);
// n factorial
//bignum* galv_fact(const bignum *n);

/*These implement more complicated functions that return irrational results
 *************************************************************************/
// base n log of a
//bignum* galv_log(const bignum *a, const bignum *n);
// e to the power of a
//bignum* galv_exp(const bignum *a);
// gamma function - an extension of factorial where gamma(n) = (n-1)!
//bignum* galv_gamma(const bignum *a);
// the real valued Riemann zeta function
//bignum* galv_zeta(const bignum *a);
// a pseudorandom number between a and b
//bignum* galv_rand(const bignum *a, const bignum *b);

// the number of decimal (base 10) digits in a
size_t galv_len(const bignum *a);
// three-way comparison of a and b - similar to strcmp
int galv_cmp(const bignum *a, const bignum *b);

/*These functions are in-place - they modify the input directly, to avoid
   allocating memory unnecessarily to create a new bignum
 ***********************************************************************/
// rounds a to n decimal places
//void galv_round(bignum *a, const bignum *n);
// rounds a up to the nearest integer
//void galv_ceil(bignum *a);
// rounds a down to the nearest integer
//void galv_floor(bignum *a);
// absolute value - removes any negative sign
//void galv_abs(bignum *a);

#ifdef __cplusplus
}
#endif

#endif /*__GALVIN_H__*/
